<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Auth::routes();
Route::get('/back-end-login', [LoginController::class, 'showLoginForm'])->name('back-end-login');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/dashboard/about', [DashboardController::class, 'about'])->name('about');
Route::post('/dashboard/about/update/{id}', [DashboardController::class, 'update_about'])->name('update_about');

Route::get('/dashboard/experience', [DashboardController::class, 'experience'])->name('experience');
Route::get('/dashboard/education', [DashboardController::class, 'education'])->name('education');
Route::get('/dashboard/skill', [DashboardController::class, 'skill'])->name('skill');
Route::get('/dashboard/awards', [DashboardController::class, 'awards'])->name('awards');
