<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPhoneToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone')->nullable()->after('email_verified_at');
            $table->string('address')->nullable()->after('phone');
            $table->string('sub_prov')->nullable()->after('address');
            $table->string('prov')->nullable()->after('sub_prov');
            $table->string('pos_code')->nullable()->after('prov');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('address');
            $table->dropColumn('sub_prov');
            $table->dropColumn('prov');
            $table->dropColumn('pos_code');
        });
    }
}
