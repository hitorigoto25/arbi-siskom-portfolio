<?php

namespace App\Http\Controllers;

use App\Models\Education;
use App\Models\Experience;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index() {
    // =========================================================================================
    // Variable Query
    // =========================================================================================
        $title = 'Home';
        $about = Auth::user();
        $name = explode(" ",$about->name);
        $experience = Experience::orderBy('created_at', 'DESC')->get();
        $education = Education::orderBy('created_at', 'DESC')->get();
    // =========================================================================================
        return view('index', compact('title','experience','education','about','name'));
    }
}
