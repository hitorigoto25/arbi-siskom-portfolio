<?php

namespace App\Http\Controllers;

use App\Models\Education;
use App\Models\Experience;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $title = 'Dashboard';
        $experience = Experience::orderBy('created_at', 'DESC')->get();
        $education = Education::orderBy('created_at', 'DESC')->get();
        $user = User::orderBy('created_at', 'DESC')->get();

        return view('admin.dashboard', compact('title','experience','education','user'));
    }

    public function about()
    {
        $title = 'About';
        $user = User::orderBy('created_at', 'DESC')->first();

        return view('admin.about', compact('title','user'));
    }

    public function update_about(Request $request, $id)
    {
        // $request->validate([
        //     'name' => 'required',
        //     'address' => 'required',
        //     'sub_prob' => 'required',
        //     'prov' => 'required',
        //     'pos_code' => 'required',
        // ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->address = $request->address;
        $user->sub_prov = $request->sub_prov;
        $user->prov = $request->prov;
        $user->pos_code = $request->pos_code;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->description = $request->description;
        $user->save();

        Alert::success('Berhasil','Data sudah diupdate!');
        return redirect()->back();
    }
}
