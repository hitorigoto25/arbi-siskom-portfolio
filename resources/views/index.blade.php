<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>{{ $title }} | Arbi Siskom</title>
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{ asset('template/dist/css/styles.css') }}" rel="stylesheet" />
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">
                <span class="d-block d-lg-none">Arbi Yudatama</span>
                <span class="d-none d-lg-block"><img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="{{ asset('template/dist/assets/img/arbi_profille.jpg') }}" alt="..." /></span>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">About</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#experience">Experience</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#education">Education</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#skills">Skills</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#interests">Interests</a></li>
                    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#awards">Awards</a></li>
                </ul>
            </div>
        </nav>
        <!-- Page Content-->
        <div class="container-fluid p-0">
            <!-- About-->
            <section class="resume-section" id="about">
                <div class="resume-section-content">
                    <h1 class="mb-0">
                        {{ $name[0] }}
                        <span class="text-primary">{{ $name[1] }}</span>
                    </h1>
                    <div class="subheading mb-5">
                        @if ($about->address == Null)
                            Tambahkan Alamat
                        @else
                            {{ $about->address }}
                        @endif
                        ·
                        @if ($about->sub_prov == Null)
                            Tambahkan Kabupaten/Kota
                        @else
                            {{$about->sub_prov}}
                        @endif
                        ,
                        @if ($about->prov == Null)
                            Tambahkan Provinsi
                        @else
                            {{ $about->prov }}
                        @endif
                        @if ($about->pos_code == Null)
                            Tambahkan Kode Pos
                        @else
                            {{ $about->pos_code }}
                        @endif
                        ·
                        @if ($about->phone == Null)
                            Tambahkan No. Handphone
                        @else
                            {{ $about->phone }}
                        @endif
                        ·
                        <a href="{{ $about->email }}">
                        @if ($about->email == Null)
                            Tambahkan Email
                        @else
                            {{ $about->email }}
                        @endif</a>
                    </div>
                    <p class="lead mb-5">{{ $about->description }}</p>
                    <div class="social-icons">
                        <a class="social-icon" href="#!"><i class="fab fa-linkedin-in"></i></a>
                        <a class="social-icon" href="#!"><i class="fab fa-github"></i></a>
                        <a class="social-icon" href="#!"><i class="fab fa-twitter"></i></a>
                        <a class="social-icon" href="#!"><i class="fab fa-facebook-f"></i></a>
                    </div>
                </div>
            </section>
            <hr class="m-0" />
            <!-- Experience-->
            <section class="resume-section" id="experience">
                <div class="resume-section-content">
                    <h2 class="mb-5">Experience</h2>
                    {{-- @if(isset()) --}}
                        @if($experience->count() == 0)
                            <p class="lead">Belum Ada Entri</p>
                        @else
                            @foreach($experience as $data)
                                <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                                    <div class="flex-grow-1">
                                        <h3 class="mb-0">{{ $data->job_name }}</h3>
                                        <div class="subheading mb-3">{{ $data->project_name }}</div>
                                        <p>{{ $data->description }}</p>
                                    </div>
                                    <div class="flex-shrink-0">
                                        <span class="text-primary">
                                            {{ Carbon\Carbon::parse($data->start_date)->translatedFormat('d F Y') }} - @if($data->end_date == Null) Now @else {{ $data->end_date }} @endif
                                        </span>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    {{-- @endif --}}
                </div>
            </section>
            <hr class="m-0" />

            <!-- Education-->
            <section class="resume-section" id="education">
                <div class="resume-section-content">
                    <h2 class="mb-5">Education</h2>
                        @foreach ($education as $data)
                            <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                                <div class="flex-grow-1">
                                    <h3 class="mb-0">{{ $data->university_name }}</h3>
                                    <div class="subheading mb-3">{{ $data->study_program }}</div>
                                    <div>{{ $data->sub_title_1 }} - {{ $data->sub_title_2 }}</div>
                                    <p>GPA: {{ $data->ipk }}</p>
                                </div>
                                <div class="flex-shrink-0">
                                    <span class="text-primary">
                                    {{ Carbon\Carbon::parse($data->start_date)->translatedFormat('d F Y') }} - @if($data->end_date == Null) Now @else {{ $data->end_date }} @endif
                                </span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>
            <hr class="m-0" />

            <!-- Skills-->
            <section class="resume-section" id="skills">
                <div class="resume-section-content">
                    <h2 class="mb-5">Skills</h2>
                    <div class="subheading mb-3">Programming Languages & Tools</div>
                    <ul class="list-inline dev-icons">
                        <li class="list-inline-item"><i class="fab fa-html5"></i></li>
                        <li class="list-inline-item"><i class="fab fa-css3-alt"></i></li>
                        <li class="list-inline-item"><i class="fab fa-js-square"></i></li>
                        {{-- <li class="list-inline-item"><i class="fab fa-angular"></i></li> --}}
                        {{-- <li class="list-inline-item"><i class="fab fa-react"></i></li> --}}
                        <li class="list-inline-item"><i class="fab fa-node-js"></i></li>
                        {{-- <li class="list-inline-item"><i class="fab fa-sass"></i></li> --}}
                        {{-- <li class="list-inline-item"><i class="fab fa-less"></i></li> --}}
                        {{-- <li class="list-inline-item"><i class="fab fa-wordpress"></i></li> --}}
                        {{-- <li class="list-inline-item"><i class="fab fa-gulp"></i></li> --}}
                        {{-- <li class="list-inline-item"><i class="fab fa-grunt"></i></li> --}}
                        <li class="list-inline-item"><i class="fab fa-npm"></i></li>
                        <li class="list-inline-item"><i class="fab fa-bootstrap"></i></li>
                        <li class="list-inline-item"><i class="fab fa-laravel"></i></li>
                    </ul>
                    <div class="subheading mb-3">Workflow</div>
                    <ul class="fa-ul mb-0">
                        <li>
                            <span class="fa-li"><i class="fas fa-check"></i></span>
                            Mobile-First, Responsive Design
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-check"></i></span>
                            Cross Browser Testing & Debugging
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-check"></i></span>
                            Cross Functional Teams
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-check"></i></span>
                            Agile Development & Scrum
                        </li>
                    </ul>
                </div>
            </section>
            <hr class="m-0" />
            <!-- Interests-->
            <section class="resume-section" id="interests">
                <div class="resume-section-content">
                    <h2 class="mb-5">Interests</h2>
                    <p>Apart from being a web developer, I enjoy most of my time being outdoors. In the winter, I am an avid skier and novice ice climber. During the warmer months here in Colorado, I enjoy mountain biking, free climbing, and kayaking.</p>
                    <p class="mb-0">When forced indoors, I follow a number of sci-fi and fantasy genre movies and television shows, I am an aspiring chef, and I spend a large amount of my free time exploring the latest technology advancements in the front-end web development world.</p>
                </div>
            </section>
            <hr class="m-0" />
            <!-- Awards-->
            <section class="resume-section" id="awards">
                <div class="resume-section-content">
                    <h2 class="mb-5">Awards & Certifications</h2>
                    <ul class="fa-ul mb-0">
                        <li>
                            <span class="fa-li"><i class="fas fa-trophy text-warning"></i></span>
                            Google Analytics Certified Developer
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-trophy text-warning"></i></span>
                            Mobile Web Specialist - Google Certification
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-trophy text-warning"></i></span>
                            1
                            <sup>st</sup>
                            Place - University of Colorado Boulder - Emerging Tech Competition 2009
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-trophy text-warning"></i></span>
                            1
                            <sup>st</sup>
                            Place - University of Colorado Boulder - Adobe Creative Jam 2008 (UI Design Category)
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-trophy text-warning"></i></span>
                            2
                            <sup>nd</sup>
                            Place - University of Colorado Boulder - Emerging Tech Competition 2008
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-trophy text-warning"></i></span>
                            1
                            <sup>st</sup>
                            Place - James Buchanan High School - Hackathon 2006
                        </li>
                        <li>
                            <span class="fa-li"><i class="fas fa-trophy text-warning"></i></span>
                            3
                            <sup>rd</sup>
                            Place - James Buchanan High School - Hackathon 2005
                        </li>
                    </ul>
                </div>
            </section>
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="{{ asset('template/dist/js/scripts.js') }}"></script>
    </body>
</html>
