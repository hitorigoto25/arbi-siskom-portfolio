<li class="sidebar-title">Menu</li>
<li class="sidebar-item active ">
    <a href="{{ url('dashboard') }}" class='sidebar-link'>
        <i class="bi bi-grid-fill"></i>
        <span>Dashboard</span>
    </a>
</li>

<li class="sidebar-item  has-sub">
    <a href="#" class='sidebar-link'>
        <i class="bi bi-collection-fill"></i>
        <span>Components</span>
    </a>
    <ul class="submenu ">
        <li class="submenu-item ">
            <a href="{{ url('dashboard/about') }}">About</a>
        </li>
        <li class="submenu-item ">
            <a href="{{ url('dashboard/experience') }}">Experience</a>
        </li>
        <li class="submenu-item ">
            <a href="{{ url('dashboard/education') }}">Education</a>
        </li>
        <li class="submenu-item ">
            <a href="{{ url('dashboard/skill') }}">Skill</a>
        </li>
        <li class="submenu-item ">
            <a href="{{ url('dashboard/awards') }}">Awards</a>
        </li>
    </ul>
</li>
